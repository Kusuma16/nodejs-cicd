const express = require('express');
const app = express();

// Routes
app.get('/', (req,res) => {
    res.send('Hello World')
});

app.get('/test', (req,res) => {
    res.send('Testing1')
});

app.get('/coba', (req,res) => {
    res.send('Testing 2')
});

// Server
app.listen(3000, () => {
    console.log('Server is running on port 3000')
})